const {io} = require('../server')

io.on('connection', (client)=> {
    console.log('usuario conectado')

    //Emitir mensaje desde el servidor a los clientes
    client.emit('enviarMensaje', {
        usuario: 'server',
        mensaje: 'Bienvenido a aplicacion socket'
    })

    client.on('disconnect', ()=> {
        console.log('usuario desconectado')

    })

    //Escuchar mensaje
    client.on('enviarMensaje', (data, callback) => {
        console.log('mensaje desde el cliente: ', data);

        client.broadcast.emit('enviarMensaje',data);

        // if(message.usuario ){
        //     callback({
        //         resp: 'todo ok'
        //     });
        // }else{
        //     callback({
        //         resp: 'no ok'
        //     });
        // }

    })
});