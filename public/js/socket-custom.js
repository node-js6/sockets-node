let socket = io();

socket.on('connect', function() {
    console.log('conectado al servidor')
});

socket.on('disconnect', function() {
    console.log('servidor desconectado')
});

//Enviar informacion al servidor
socket.emit('enviarMensaje', {
    usuario: 'wilmer',
    mensaje: 'hello world'
},function (res) {
    console.log('respuesta server:' ,res)
});

//escuchar informacion del servidor
socket.on('enviarMensaje', function(mensaje){
    console.log(mensaje)
})